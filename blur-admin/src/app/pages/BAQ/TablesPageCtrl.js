/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.BAQ')
        .controller('TablesPageCtrl', TablesPageCtrl);
  
    /** @ngInject */
    function TablesPageCtrl(composeModal, TablesPage) {
  
      var vm = this;
      
  
      vm.users = TablesPage.getUsers();
    }
  
  })();
  