/**
 * @author a.demeshko
 * created on 1/12/16
 */
(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.BAQ.Application', [])
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
        .state('BAQ.Application', {
          url: '/application',
          templateUrl: 'app/pages/BAQ/Application/Application.html',
            title: 'Application',
            controller: 'TablesPageCtrl',
            sidebarMeta: {
              icon: 'ion-ios-pulse',
              order: 0,
            },
        });
    }
  })();