/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.BAQ', [
        'BlurAdmin.pages.BAQ.Secteur',
        'BlurAdmin.pages.BAQ.globalView',
        
    ])
        .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
          .state('BAQ', {
            url: '/BAQ',
            abstract: true,
            template: '<div ui-view  autoscroll="true" autoscroll-body-top></div>',
            Controller:'TablesPageCtrl',
            ControllerAs:'PageCtrl',
            title: 'BAQ',
            sidebarMeta: {
              icon: 'ion-stats-bars',
              order: 50,
            },
          });
    }
  
  })();
  