/**
 * @author a.demeshko
 * created on 1/12/16
 */
(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.BAQ.globalView', [])
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
        .state('BAQ.globalView', {
          url: '/globalView',
          templateUrl: 'app/pages/BAQ/globalView/globalView.html',
            title: 'globalView',
            sidebarMeta: {
              icon: 'ion-ios-pulse',
              order: 100,
            },
        });
    }
  })();