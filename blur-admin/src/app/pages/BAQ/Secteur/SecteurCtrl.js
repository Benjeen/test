
  /**
 * @author a.demeshko
 * created on 28.12.2015
 */
(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.BAQ')
      .controller('SecteurCtrl', SecteurCtrl);
  
    /** @ngInject */
    function SecteurCtrl($stateParams, TablesPage) {
      var vm = this;
      vm.users= TablesPage.getUsers();
      vm.user=TablesPage.getUserById($stateParams.id)
    }
  
  })();
  