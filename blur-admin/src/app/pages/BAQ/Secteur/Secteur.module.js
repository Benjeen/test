/**
 * @author a.demeshko
 * created on 1/12/16
 */
(function () {
    'use strict';
  
    angular.module('BlurAdmin.pages.BAQ.Secteur', [])
      .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
      $stateProvider
        .state('BAQ.Secteur', {
          url: '/secteur',
          templateUrl: 'app/pages/BAQ/Secteur/Secteur.html',
            title: 'Secteur',
            controller: 'SecteurCtrl',
            controllerAs:'SeCtrl',
            sidebarMeta: {
              icon: 'ion-ios-pulse',
              order: 100,
            },
        });
    }
  })();